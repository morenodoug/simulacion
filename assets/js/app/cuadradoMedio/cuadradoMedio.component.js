angular.
module('cuadradoMedio').
component('cuadradoMedioComponent',{
  templateUrl: 'js/app/cuadradoMedio/cuadradoMedio.template.html',
  controller: [ function(){

    var self = this;

    function algoritmoCuadrado(semilla,generarCantidad){

      if (generarCantidad ==null) {
        generarCantidad =1;

      }
      var numerosGenerados = [];
      for (var i = 0; i < generarCantidad; i++) {

        var largoSemilla = semilla.toString().length;
        var semillaCuadrada = semilla* semilla;
        var stringSemillaCuadrada   = semillaCuadrada.toString();

        if ((stringSemillaCuadrada.length %2) !=0 ) {
          stringSemillaCuadrada = '0'+stringSemillaCuadrada;
        }
        var posIniCadena  = (largoSemilla/2);
        var posFinalCadena =  posIniCadena + largoSemilla;
        var nuevaSemilla = parseInt (stringSemillaCuadrada.substring(posIniCadena,posFinalCadena  ));
        var numeroAleatorio = nuevaSemilla/(Math.pow(10,largoSemilla));
        numerosGenerados.push({
          'semilla': semilla,
          'numeroAleatorio':numeroAleatorio,
          posicion: i+1

        });
        console.log('semillacuadrada: ' +stringSemillaCuadrada);
        console.log('tomar numero: '+stringSemillaCuadrada.substring(posIniCadena,posFinalCadena  ));
        console.log('aletorio: ' + numeroAleatorio );
        console.log('');
        console.log('');
        semilla = nuevaSemilla;
      }
      var objeto = {
        data: numerosGenerados,
        length: generarCantidad
      }
      return objeto;
    }

    self.generarNumerosCuadradoMedio= function(semilla, generarCantidad){
      self.generados = algoritmoCuadrado(semilla,generarCantidad);
    }
    //self.generados = algoritmoCuadrado(101,7);

    self.selected = [];

    self.query = {
      order: '',
      limit: 5,
      page: 1
    };


  }]
})
