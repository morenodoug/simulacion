angular.
module('productoMedio').
component('productoMedioComponent',{
  templateUrl: 'js/app/productoMedio/productoMedio.template.html',
  controller: [ function(){

    var self = this;

    function algoritmoProductosMedio(r1,r2,generarCantidad){

      if (generarCantidad ==null) {
        generarCantidad =1;

      }
      var numerosGenerados= [];
      for (var i = 0; i <generarCantidad ; i++) {


        var multiplicacion = r1*r2;
        var largoR1 = r1.toString().length;
        var stringMultiplicacion = multiplicacion.toString();
        var largoMultiplicacion = stringMultiplicacion.length;

        if ((largoMultiplicacion %2) !=0 ) {
          stringMultiplicacion = '0'+stringMultiplicacion;
        }

        var posIniCadena  = (largoR1/2);
        var posFinalCadena =  posIniCadena + largoR1;
        var r3 =  parseInt (stringMultiplicacion.substring(posIniCadena,posFinalCadena  ));
        numerosGenerados.push({
          'numeroAleatorio':r3,
          'posicion': i+1,
          'r1':r1,
          'r2':r2
        });
        console.log('r1: '+ r1);
        console.log('r2: '+ r2);
        console.log('r1*r2: '+ stringMultiplicacion);
        console.log('generado: '+r3);
        r1=r2;
        r2=r3;
      }

      var obj= {
        'data':numerosGenerados,
        'length':generarCantidad
      }
      return obj;

    }

    self.generarNumerosProductoMedio= function(r1,r2, generarCantidad){
      self.generados = algoritmoProductosMedio(r1,r2,generarCantidad);
    }
    //self.generados = algoritmoCuadrado(101,7);

    self.selected = [];

    self.query = {
      order: '',
      limit: 5,
      page: 1
    };


  }]
})
