
'use strict';
angular.
	module('customvalidation').
	directive('tamanopar',function () {

		return {
			require:'ngModel',
			link: function (scope,elm,attrs,ctrl) {

        ctrl.$validators.tamanopar = function(modelValue,viewValue){
            
          if (!viewValue) {
            return false;
          }
          if(viewValue.toString().length % 2 == 0){
              return true;
          }
          return false;

		        };
			}
		};
	});
