'use strict';
angular.
	module('customvalidation').
	directive('integer',function () {
		var INTEGER_REGEXP = /^\d+$/;
		return {
			require:'ngModel',
			link: function (scope,elm,attrs,ctrl) {

				ctrl.$validators.integer = function(modelValue,viewValue){

			        if (INTEGER_REGEXP.test(viewValue)) {
			          // it is valid
			          //elm.css('background-color','white');
			          // console.log('true:  '+viewValue);
			          return true;
			        }
			        // it is invalid
			        //elm.css('background-color','red');
			        // console.log('false:   ' +viewValue);
			        return false;

		        };
			}
		};
	});
