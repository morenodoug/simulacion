
'use strict';
angular.
	module('customvalidation').
	directive('cantidaddigitos',function () {

		return {
			require:'ngModel',
			link: function (scope,elm,attrs,ctrl) {

        ctrl.$validators.cantidaddigitos = function(modelValue,viewValue){
					// console.log(attrs.cantidaddigitos);
					if (!attrs.cantidaddigitos || !viewValue) {
						return false;
					}
					var r1Length = attrs.cantidaddigitos.toString().length;
					var r2Length = viewValue.toString().length;
					if (r1Length == r2Length) {
						return true; //valid

					}
					return false;	//not valid

		    };
			}
		};
	});
